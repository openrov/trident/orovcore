#pragma once

#include <string>
#include <vector>

#include <dds/dds.hpp>

namespace orov {
namespace core {

    // Common QOS Profile Names
    namespace qos {
        // Participant Level
        constexpr auto PARTICIPANT_COMMON                   = "OpenROV::Participant.Common";
        constexpr auto PARTICIPANT_VEHICLE                  = "OpenROV::Participant.Vehicle";

        // Reader/Writer Level
        constexpr auto BEST_EFFORT                          = "OpenROV::Generic.BestEffort";
        constexpr auto KEEP_LAST_RELIABLE                   = "OpenROV::Generic.KeepLastReliable";
        constexpr auto KEEP_LAST_RELIABLE_TRANSIENT_LOCAL   = "OpenROV::Generic.KeepLastReliable.TransientLocal";
        constexpr auto LIVE_VIDEO                           = "OpenROV::LiveVideo";
    }

    class Node 
    {
    public:
        Node(   const std::string& nodeName, 
                const std::vector<std::string>& partitions,
                const std::vector<std::string>& qosFilePaths,
                const std::string& defaultLib, 
                const std::string& defaultProfile, 
                int domain = 0 );

        ~Node() = default;

        // Non-copyable
        Node( const Node& ) = delete;
        Node( Node&& ) = delete;
        Node& operator=( const Node& ) = delete;

        // Getters
        const std::string&                      Name() const;

        dds::domain::DomainParticipant          DefaultParticipant() const;
        dds::pub::Publisher                     DefaultPublisher() const;
        dds::sub::Subscriber                    DefaultSubscriber() const;

        // Qos Profile Factory Methods
        dds::domain::qos::DomainParticipantQos  CreateParticipantQos( const std::string& profileName ) const;
        dds::pub::qos::DataWriterQos            CreateWriterQos( const std::string& profileName ) const;
        dds::sub::qos::DataReaderQos            CreateReaderQos( const std::string& profileName ) const;

        // Topic Factory Method
        template<typename T>
        static dds::topic::Topic<T> CreateTopic( dds::domain::DomainParticipant participant, const std::string& topicName )
        {
            // First attempt to find an existing topic with this name and type in the participant
            // TODO: This may not be threadsafe
            auto topic = dds::topic::find<dds::topic::Topic<T>>( participant, topicName );
            if( dds::core::null == topic )
            {
                // Topic did not exist, so create it now
                topic = dds::topic::Topic<T>( participant, topicName );
            }

            return topic;
        }

        // Writer Factory method
        template<typename T>
        dds::pub::DataWriter<T> CreateWriter(const std::string& topicName, const std::string& qosString) const {
            // Participant
            const auto &participant = DefaultParticipant();

            // publisher
            const auto &publisher = DefaultPublisher();

            // Get the topic
            dds::topic::Topic<T> topic = CreateTopic<T>(participant, topicName);

            // Setup the QoS's
            const auto &qos = dds::core::QosProvider::Default()->datawriter_qos(qosString);

            return dds::pub::DataWriter<T>(publisher, topic, qos);
        }

        // Reader Factory method
        template<typename T>
        dds::sub::DataReader<T> CreateReader(const std::string& topicName, const std::string& qosString) const {
            // Participant
            const auto& participant = DefaultParticipant();

            // Subscriber
            const auto& subscriber = DefaultSubscriber();

            // Get the topic
            dds::topic::Topic<T> topic = CreateTopic<T>(participant, topicName);

            // Setup the QoS's
            const auto& qos = dds::core::QosProvider::Default()->datareader_qos(qosString);

            return dds::sub::DataReader<T>(subscriber, topic, qos);
        }


    private:
        const std::string                       m_name;

        // All nodes get created with at least one participant, publisher, and subscriber.
        // The user is not prevented from creating more, if they need to.
        dds::domain::DomainParticipant          m_defaultPart;
        dds::pub::Publisher                     m_defaultPub;
        dds::sub::Subscriber                    m_defaultSub;

        void SetupQos(  const std::vector<std::string>& qosFilePaths,
                        const std::string& defaultLib, 
                        const std::string& defaultProfile );
    };

}
}