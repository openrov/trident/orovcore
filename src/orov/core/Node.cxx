#include <orov/core/Node.hpp>

namespace orov {
namespace core {

Node::Node( const std::string& nodeName, 
            const std::vector<std::string>& partitions,
            const std::vector<std::string>& qosFilePaths,
            const std::string& defaultLib, 
            const std::string& defaultProfile, 
            int domain )
    : m_name{ nodeName }
    , m_defaultPart{ nullptr }
    , m_defaultPub{ nullptr }
    , m_defaultSub{ nullptr }
{
    SetupQos( qosFilePaths, defaultLib, defaultProfile );

    // Create participant
    auto partQos = dds::core::QosProvider::Default()->participant_qos();
    partQos.policy<rti::core::policy::EntityName>().name( nodeName );
    m_defaultPart = dds::domain::DomainParticipant( domain, partQos );

    // Set up default publisher and subscriber QOS with partitions
    auto pubQos = m_defaultPart.default_publisher_qos();
    auto subQos = m_defaultPart.default_subscriber_qos();

    // Fix empty partition list and invalid wildcard specification
    if( partitions.empty() || ( partitions.size() == 1 && partitions[ 0 ] == "*" ) )
    {
        pubQos << dds::core::policy::Partition{ std::vector<std::string>{ "", "*" } };
        subQos << dds::core::policy::Partition{ std::vector<std::string>{ "", "*" } };
    }
    else
    {
        pubQos << dds::core::policy::Partition( partitions );
        subQos << dds::core::policy::Partition( partitions );
    }

    m_defaultPart.default_publisher_qos( pubQos );
    m_defaultPart.default_subscriber_qos( subQos );

    // Create publisher and subscriber
    m_defaultPub = dds::pub::Publisher( m_defaultPart );
    m_defaultSub = dds::sub::Subscriber( m_defaultPart );
}

void Node::SetupQos(    const std::vector<std::string>& qosFilePaths,
                        const std::string& defaultLib, 
                        const std::string& defaultProfile )
{
    // Load Qos Profiles and set default library and profile
    if( !qosFilePaths.empty() )
    {
        rti::core::QosProviderParams params;
        params.url_profile( qosFilePaths );

        dds::core::QosProvider::Default()->default_provider_params( params );

        if( "" != defaultLib && "" != defaultProfile )
        {
            dds::core::QosProvider::Default()->default_library( defaultLib );
            dds::core::QosProvider::Default()->default_profile( defaultProfile );
        }
    }
}

dds::domain::DomainParticipant Node::DefaultParticipant() const
{
    return m_defaultPart;
}

dds::pub::Publisher Node::DefaultPublisher() const
{
    return m_defaultPub;
}

dds::sub::Subscriber Node::DefaultSubscriber() const
{
    return m_defaultSub;
}

}
}