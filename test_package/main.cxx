#include <dds/dds.hpp>

#include <orov/core/Node.hpp>
#include <orov/core/Logging.hpp>

#include <chrono>
#include <thread>

int main()
{
    auto logger = orov::core::CreateProductionLogger( "example", "/tmp" );

    logger->info( "hey!" );
    logger->error( "hey!" );

    spdlog::info( "guys!" );
    spdlog::error( "guys!" );

    spdlog::trace( "oh nooo" );
    
    // Create node
    orov::core::Node node{
        "example-node",
        { "1707e20" },
        {},
        "",
        ""
    };

    // Create topic
    auto topic = orov::core::Node::CreateTopic<dds::core::StringTopicType>( node.DefaultParticipant(), "test_topic" );

    // Create writer qos
    auto qos = dds::core::QosProvider::Default()->datawriter_qos();
    qos.policy<dds::core::policy::Reliability>( dds::core::policy::Reliability::Reliable() );
    qos.policy<dds::core::policy::Durability>( dds::core::policy::Durability::TransientLocal() );
    qos.policy<dds::core::policy::History>( dds::core::policy::History::KeepLast( 1 ) );

    // Create writer
    dds::pub::DataWriter<dds::core::StringTopicType> writer( node.DefaultPublisher(), topic, qos );

    // Write a message
    dds::core::StringTopicType msg{};
    writer << msg;

    return 0;
}