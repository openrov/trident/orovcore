from conans import ConanFile, CMake

class OrovcoreConan( ConanFile ):
    name            = "orovcore"
    version         = "1.1.0"
    license         = "MIT"
    url             = "https://gitlab.com/openrov/trident/orovcore"
    description     = "Helper library for quickly and easily creating DDS-based applications within the OpenROV software ecosystem"

    exports_sources = "include/*", "src/*", "CMakeLists.txt"
    settings        = "os", "arch", "compiler", "build_type", "arch_build"
    requires        = ( 
                        ( "rticonnextpro/6.0.0@openrov/stable" ),
                        ( "spdlog/1.3.1@bincrafters/stable" ),
                        ( "boost/1.70.0@conan/stable" ) 
                    )

    generators      = "cmake"

    def build( self ):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()
    
    def package( self ):
        self.copy( "include/*", dst="", keep_path=True )
        self.copy( "*.a", dst="lib", keep_path=False )

    def package_info( self ):
        self.cpp_info.includedirs   = [ 'include' ]
        self.cpp_info.libdirs       = [ 'lib' ]

        self.cpp_info.libs          = [ 'orovcore' ]