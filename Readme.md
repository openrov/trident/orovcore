# orovcore

Core functionality for OpenROV. 

## How to build

This is built inside the Docker container built in openrov-build-containers. using the
'run.sh' shell command ('dev' alias) from this directory:

```sh
cd workspace
mkdir build && cd build
conan install ..
conan build ..
```
